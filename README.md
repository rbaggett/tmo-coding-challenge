## Task 1

> What is done well?

1. Lazy loading is in place.
2. Modular code design.
3. Separation of responsibilities.


> What would you change?

I understand that this is an exercise, however I take it at face value. Please pardon any critiques for content meant to enable easier consumption of this exercise.

1. I would not group the API and the Frontend into a single GitLab project. A change to the api (or the frontend) means a fresh build in the pipeline for the entire code base; instead of just the relevant code.
2. I've worked at a large company with teams that change, and being so, my style tends to favor avoiding complexity in favor of transparency. That becomes especially important when support comes into the picture. Having said that, I do feel like the project nesting structure is a bit deep.


> Are there any code smells or problematic implementations?

1. Changed the googleChart component to toggle visiblity based on chartData, instead of data.
2. No code documentation.
3. Cleaned up unused declarations in chart component.
4. Added cleanup Destroy logic in chart component.
5. Moved FormGroup logic to ngOnInit in stocks component.
